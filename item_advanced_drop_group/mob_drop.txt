// Monster Drop Database
// Add drop item to monster
//
// Structure:
// <mobid>,<itemid>,<rate>{,<randopt_groupid>,<flag>}
//
// <mobid>  : Monster ID. See db/[pre-]re/mob_db.txt
// <itemid> : Item ID.
// <rate>   : 1 = 0.01%
//            100 = 1%
//            10000 = 100%
//            Just like rate in mob_db.txt, adjusted by battle_config.
//            To remove original drop from monster, use 0 as rate.
// Optional:
// <randopt_groupid> : If set, the dropped item will be modified by Random Option Group based on db/[pre-]re/item_randomopt_group.txt
// <flag>            : 1 - The item is protected from steal.
//                     2 - As MVP Reward

// Test weapon
1002,1202,500,RDMOPTG_Newbie_WeaponCard	// PORING -> Knife_
1007,1602,500,RDMOPTG_Newbie_WeaponCard // FABRE -> Rod_
1010,1702,500,RDMOPTG_Newbie_WeaponCard	// WILLOW -> Bow_
1063,1102,500,RDMOPTG_Newbie_WeaponCard // LUNATIC -> Sword_
// Test Armor
1088,2304,500,RDMOPTG_Newbie_ArmorCard	// VOCAL -> Leather_Jacket_
1089,2308,500,RDMOPTG_Newbie_ArmorCard	// TOAD -> Mantle_
1090,2302,500,RDMOPTG_Newbie_ArmorCard	// MASTERING -> Cotton_Shirt_
1093,2306,500,RDMOPTG_Newbie_ArmorCard	// ECLIPSE -> Adventurere's_Suit_

1113,28107,10000,RDMOPTG_ViciousMind_Physic	// DROPS -> Two_Handed_Axe_of_Vicious_Mind
1113,28706,10000,RDMOPTG_ViciousMind_Physic2	// DROPS -> Dagger_of_Vicious_Mind
1031,21016,10000,RDMOPTG_ViciousMind_Physic3	// POPORING -> Two_Handed_Sword_of_Vicious_Mind
1031,1400,10000,RDMOPTG_ViciousMind_Physic3	// POPORING -> Spear_of_Vicious_Mind
1242,2026,10000,RDMOPTG_ViciousMind_Magic	// MARIN -> Staff_of_Vicious_Mind
1242,1600,10000,RDMOPTG_ViciousMind_Magic	// MARIN -> Rod_of_Vicious_Mind
1242,16041,10000,RDMOPTG_ViciousMind_Magic	// MARIN -> Mace_of_Vicious_Mind
1613,18121,10000,RDMOPTG_ViciousMind_Range	// METALING -> Bow_of_Vicious_Mind
1613,13128,10000,RDMOPTG_ViciousMind_Range	// METALING -> Revolver_of_Vicious_Mind

// Vicious Mind weapons ofc the mobid must be wrong!
//3474,21016,50,RDMOPTG_ViciousMind_Physic3	// AS_BLOODY_KNIGHT/Immortal Castle Guard -> Two_Handed_Sword_of_Vicious_Mind
//3483,21016,50,RDMOPTG_ViciousMind_Physic3	// AS_EVIL_SHADOW3/Immortal Shadow3 -> Two_Handed_Sword_of_Vicious_Mind
//3474,1450,50,RDMOPTG_ViciousMind_Physic3	// AS_BLOODY_KNIGHT/Immortal Castle Guard -> Lance_of_Vicious_Mind
//3483,1450,50,RDMOPTG_ViciousMind_Physic3	// AS_EVIL_SHADOW3/Immortal Shadow3 -> Lance_of_Vicious_Mind
//3479,28107,50,RDMOPTG_ViciousMind_Physic	// AS_ZOMBIE_MASTER/Immortal Zombie Master -> Two_Handed_Axe_of_Vicious_Mind
//3481,28107,50,RDMOPTG_ViciousMind_Physic	// AS_EVIL_SHADOW1/Immortal Shadow1 -> Two_Handed_Axe_of_Vicious_Mind
//3480,2026,50,RDMOPTG_ViciousMind_Magic	// AS_CURSED_SOLDIER/Immortal Cursed Zombie -> Staff_of_Vicious_Mind
//3482,2026,50,RDMOPTG_ViciousMind_Magic	// AS_EVIL_SHADOW2/Immortal Shadow2 -> Staff_of_Vicious_Mind
//3479,28008,50,RDMOPTG_ViciousMind_Physic3	// AS_ZOMBIE_MASTER/Immortal Zombie Master -> Katar_of_Vicious_Mind
//3481,28008,50,RDMOPTG_ViciousMind_Physic3	// AS_EVIL_SHADOW1/Immortal Shadow1 -> Katar_of_Vicious_Mind
//3479,13328,50,RDMOPTG_ViciousMind_Physic2	// AS_ZOMBIE_MASTER/Immortal Zombie Master -> Huuma_Shuriken_of_Vicious_Mind
//3481,13328,50,RDMOPTG_ViciousMind_Physic2	// AS_EVIL_SHADOW1/Immortal Shadow1 -> Huuma_Shuriken_of_Vicious_Mind
//3474,28706,50,RDMOPTG_ViciousMind_Physic2	// AS_BLOODY_KNIGHT/Immortal Castle Guard -> Dagger_of_Vicious_Mind
//3483,28706,50,RDMOPTG_ViciousMind_Physic2	// AS_EVIL_SHADOW3/Immortal Shadow3 -> Dagger_of_Vicious_Mind
//3474,13455,50,RDMOPTG_ViciousMind_Physic	// AS_BLOODY_KNIGHT/Immortal Castle Guard -> Saber_of_Vicious_Mind
//3483,13455,50,RDMOPTG_ViciousMind_Physic	// AS_EVIL_SHADOW3/Immortal Shadow3 -> Saber_of_Vicious_Mind
//3474,1400,50,RDMOPTG_ViciousMind_Physic3	// AS_BLOODY_KNIGHT/Immortal Castle Guard -> Spear_of_Vicious_Mind
//3483,1400,50,RDMOPTG_ViciousMind_Physic3	// AS_EVIL_SHADOW3/Immortal Shadow3 -> Spear_of_Vicious_Mind
//3479,16041,50,RDMOPTG_ViciousMind_Magic	// AS_ZOMBIE_MASTER/Immortal Zombie Master -> Mace_of_Vicious_Mind
//3481,16041,50,RDMOPTG_ViciousMind_Magic	// AS_EVIL_SHADOW1/Immortal Shadow1 -> Mace_of_Vicious_Mind
//3480,1600,50,RDMOPTG_ViciousMind_Magic	// AS_CURSED_SOLDIER/Immortal Cursed Zombie -> Rod_of_Vicious_Mind
//3482,1600,50,RDMOPTG_ViciousMind_Magic	// AS_EVIL_SHADOW2/Immortal Shadow2 -> Rod_of_Vicious_Mind
//3480,1900,50,RDMOPTG_ViciousMind_Range	// AS_CURSED_SOLDIER/Immortal Cursed Zombie -> Violin_of_Vicious_Mind
//3482,1900,50,RDMOPTG_ViciousMind_Range	// AS_EVIL_SHADOW2/Immortal Shadow2 -> Violin_of_Vicious_Mind
//3480,1996,50,RDMOPTG_ViciousMind_Range	// AS_CURSED_SOLDIER/Immortal Cursed Zombie -> Wire_of_Vicious_Mind
//3482,1996,50,RDMOPTG_ViciousMind_Range	// AS_EVIL_SHADOW2/Immortal Shadow2 -> Wire_of_Vicious_Mind
//3479,28605,50,RDMOPTG_ViciousMind_Physic2	// AS_ZOMBIE_MASTER/Immortal Zombie Master -> Book_of_Vicious_Mind
//3481,28605,50,RDMOPTG_ViciousMind_Physic2	// AS_EVIL_SHADOW1/Immortal Shadow1 -> Book_of_Vicious_Mind
//3479,1800,50,RDMOPTG_ViciousMind_Physic3	// AS_ZOMBIE_MASTER/Immortal Zombie Master -> Fist_of_Vicious_Mind
//3481,1800,50,RDMOPTG_ViciousMind_Physic3	// AS_EVIL_SHADOW1/Immortal Shadow1 -> Fist_of_Vicious_Mind
//3480,13128,50,RDMOPTG_ViciousMind_Range	// AS_CURSED_SOLDIER/Immortal Cursed Zombie -> Revolver_of_Vicious_Mind
//3482,13128,50,RDMOPTG_ViciousMind_Range	// AS_EVIL_SHADOW2/Immortal Shadow2 -> Revolver_of_Vicious_Mind
//3480,18121,50,RDMOPTG_ViciousMind_Range	// AS_CURSED_SOLDIER/Immortal Cursed Zombie -> Bow_of_Vicious_Mind
//3482,18121,50,RDMOPTG_ViciousMind_Range	// AS_EVIL_SHADOW2/Immortal Shadow2 -> Bow_of_Vicious_Mind
