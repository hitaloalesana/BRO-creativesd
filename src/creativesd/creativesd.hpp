#ifndef _CREATIVESDMAIN_HPP_
#define _CREATIVESDMAIN_HPP_

extern struct CreativeSD_Config {
	// User Id
	int user_id;

	// Users and Password
	char username[40], password[32];

	// Event Room
	char eventroom_map[12];
	int eventroom_x;
	int eventroom_y;
} creativesd_config;

int do_init_creativesd();
int do_final_creativesd();

#endif /* _CREATIVESDMAIN_HPP_ */
