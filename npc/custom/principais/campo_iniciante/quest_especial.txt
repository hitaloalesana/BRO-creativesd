new_zone01,138,77,6	script	Quests Especiais	71,{
	mes set(.@n$,"^0099FF[ "+strnpcinfo(1)+" ]^000000");
	mes "Que tal realizar uma quest diferente? Esta requer habilidades espeicias, ter� que ser apto a derrotar muitos monstros.";
	mes "Abaixo voce poder� ver quais delas j� realizou, lembrando que � apenas uma vez cada uma delas.";
	mes "- Item A: "+((#item_a) ? "^00BB22J� Realizada^000000":"^FF2200N�o Realizada^000000");
	mes "- Item B: "+((#item_b) ? "^00BB22J� Realizada^000000":"^FF2200N�o Realizada^000000");
	mes "- Item C: "+((#item_c) ? "^00BB22J� Realizada^000000":"^FF2200N�o Realizada^000000");
	mes "- Item D: "+((#item_d) ? "^00BB22J� Realizada^000000":"^FF2200N�o Realizada^000000");
	next;
	switch(select("^00CC00�^000000 Fazer uma Quest.:^00CC00�^000000 Cancelar.")){
		case 1:
			mes .@n$;
			mes "Escolha na lista a quest voc� deseja fazer:";
			next;
			switch(select("Item A ["+((#item_a) ? "^00BB22J� Realizada^000000":"^FF2200N�o Realizada^000000")+"]","Item B ["+((#item_b) ? "^00BB22J� Realizada^000000":"^FF2200N�o Realizada^000000")+"]","Item C ["+((#item_c) ? "^00BB22J� Realizada^000000":"^FF2200N�o Realizada^000000")+"]","Item D ["+((#item_d) ? "^00BB22J� Realizada^000000":"^FF2200N�o Realizada^000000")+"]","Cancelar")){
				case 1:
					mes .@n$;
					if(#item_a){
						mes "Voc� j� realizou essa quest.";
						close;
					}
					mes "- Monstro a ser derrotado: ^2596C2"+ getmonsterinfo( 1002,MOB_NAME ) +"^000000"; 
					mes "- Derrote: ^FF00001.000x monstros^000000";
					mes "- Derrotados: ^00BB22"+ check_item_a +"^000000";
					next;
					if(select("^00CC00�^000000 Finalizar Quest.:^00CC00�^000000 Sa�r.") == 2) close;
						if(check_item_a < 10){
							mes "^2596C2Voc� precisa matar mais monstros para conclu�r a quest.^000000"; 
							close; 
						}
						mes .@n$;
						mes "Quest realizada com sucesso, parab�ns!";
						getitem 607,1;
						set #item_a,1;
						close;
				case 2:
					mes .@n$;
					if(#item_b){ 
						mes "Voc� j� realizou esta quest.";
						close; 
					}
					mes "- Monstro a ser derrotado: ^2596C2"+ getmonsterinfo( 1003,MOB_NAME ) +"^000000"; 
					mes "- Derrote: ^FF00001.000x monstros^000000";
					mes "- Derrotados: ^00BB22"+ check_item_b +"^000000"; 
					next;
					if(select("^00CC00�^000000 Finalizar Quest.:^00CC00�^000000 Sa�r.") == 2) close;
						if(check_item_b < 10){ 
							mes "^2596C2Voc� precisa matar mais monstros para conclu�r a quest.^000000"; 
							close; 
						}
						mes .@n$;
						mes "Quest realizada com sucesso, parab�ns!";
						getitem 607,1;
						set #item_b,1;
						close;
				case 3:
					mes .@n$;
					if(#item_c){
						mes "Voc� j� adqu�riu este buff.";
						close;
					}
					mes "- Monstro a ser derrotado: ^2596C2"+ getmonsterinfo( 1004,MOB_NAME ) +"^000000"; 
					mes "- Derrote: ^FF00001.000x monstros^000000";
					mes "- Derrotados: ^00BB22"+ check_item_c +"^000000";
					next;
					if(select("^00CC00�^000000 Finalizar Quest.:^00CC00�^000000 Sa�r.") == 2) close;
						if(check_item_c < 10){ 
							mes "^2596C2Voc� precisa matar mais monstros para conclu�r a quest.^000000"; 
							close; 
						}
						mes .@n$;
						mes "Quest realizada com sucesso, parab�ns!";
						getitem 607,1;
						set #item_c,1;
						close;
				case 4:
					mes .@n$;
					if(#item_d){
						mes "Voc� j� adqu�riu este buff.";
						close;
					}
					mes "- Monstro a ser derrotado: ^2596C2"+ getmonsterinfo( 1005,MOB_NAME ) +"^000000"; 
					mes "- Derrote: ^FF00001.000x monstros^000000";
					mes "- Derrotados: ^00BB22"+ check_item_d +"^000000";
					next;
					if(select("^00CC00�^000000 Finalizar Quest.:^00CC00�^000000 Sa�r.") == 2) close;
						if(check_item_d < 10){ 
							mes "^2596C2Voc� precisa matar mais monstros para conclu�r a quest.^000000"; 
							close; 
						}
						mes .@n$;
						mes "Quest realizada com sucesso, parab�ns!";
						getitem 607,1;
						set #item_d,1;
						close;
				case 5:
					mes .@n$;
					mes "Volte quando quiser!";
					close;
		}
		case 2:
			mes .@n$;
			mes "Volte quando quiser!";
			close;
OnNPCKillEvent:
	if(killedrid == 1002){
		if( check_item_a < 10 ){
			set check_item_a,check_item_a + 1; 
			message strcharinfo(0), "Quest conclu�da, j� pode retirar seu item!",0x000000;
		} else {
			dispbottom "Voc� j� matou 10 "+ getmonsterinfo( 1002,MOB_NAME ) +", tenha mais chance em dropar Moeda Poring!",0x000000;
			end;
		end; 
	}	
	if(killedrid == 1003){ 
		if( check_item_b < 10 ){
			set check_item_b,check_item_b + 1; 
			message strcharinfo(0), "Quest conclu�da, j� pode retirar seu item!",0x000000;
		} else {
			dispbottom "Voc� j� matou 10 "+ getmonsterinfo( 1003,MOB_NAME ) +", tenha mais chance em dropar Moeda Poring!",0x000000;
			end;
		end; 
	}	
	if(killedrid == 1004){ 
		if( check_item_c < 10 ){
			set check_item_c,check_item_c + 1; 
			message strcharinfo(0), "Quest conclu�da, j� pode retirar seu item!",0x000000;
		} else {
			dispbottom "Voc� j� matou 10 "+ getmonsterinfo( 1004,MOB_NAME ) +", tenha mais chance em dropar Moeda Poring!",0x000000;
			end;
		end; 
	}	
	if(killedrid == 1005){ 
		if( check_item_d < 10 ){
			set check_item_d,check_item_d + 1; 
			message strcharinfo(0), "Quest conclu�da, j� pode retirar seu item!",0x000000;
		} else {
			dispbottom "Voc� j� matou 10 "+ getmonsterinfo( 1005,MOB_NAME ) +", tenha mais chance em dropar Moeda Poring!",0x000000;
			end;
		end; 
	}
	end;
}