-	script	crazy_run	-1,{
// Inicio das Configura��es:
OnInit:
	.tempoEvento = 10;						// Tempo de dura��o do evento (Padr�o = [ 6 Minutos ] ).
	$@CrazyRun = 0;							// V�ri�vel do evento (N�o mexer).
	$@CrazyRunEnter = 0;					// V�ri�vel de entrada do evento (N�o mexer).
	.anuncio = 3;							// Tempo de espera para iniciar evento (Padr�o = [ 3 Minutos ] ).
	.mapa$ = "turbo_n_1";					// Mapa do evento.
	end;
	
OnStartEvent:
	// monstros
	killmonsterall .mapa$;
	monster "turbo_n_1",19,90,"N�o vai subir ningu�m",1512,2;
	monster "turbo_n_1",19,90,"N�o vai subir ningu�m",1026,2;
	monster "turbo_n_1",19,90,"N�o vai subir ningu�m",1188,2;
	monster "turbo_n_1",56,72,"N�o vai subir ningu�m",1512,2;
	monster "turbo_n_1",56,72,"N�o vai subir ningu�m",1026,2;
	monster "turbo_n_1",56,72,"N�o vai subir ningu�m",1188,2;
	monster "turbo_n_1",29,53,"N�o vai subir ningu�m",1512,2;
	monster "turbo_n_1",29,53,"N�o vai subir ningu�m",1026,2;
	monster "turbo_n_1",29,53,"N�o vai subir ningu�m",1188,2;
	monster "turbo_n_1",41,25,"N�o vai subir ningu�m",1512,2;
	monster "turbo_n_1",41,25,"N�o vai subir ningu�m",1026,2;
	monster "turbo_n_1",41,25,"N�o vai subir ningu�m",1188,2;
	monster "turbo_n_1",73,50,"N�o vai subir ningu�m",1512,2;
	monster "turbo_n_1",73,50,"N�o vai subir ningu�m",1026,2;
	monster "turbo_n_1",73,50,"N�o vai subir ningu�m",1188,2;
	monster "turbo_n_1",79,19,"N�o vai subir ningu�m",1512,2;
	monster "turbo_n_1",79,19,"N�o vai subir ningu�m",1026,2;
	monster "turbo_n_1",79,19,"N�o vai subir ningu�m",1188,2;
	monster "turbo_n_1",278,284,"ATIRADOR DO BOPE",1016,1;
	monster "turbo_n_1",278,264,"ATIRADOR DO BOPE",1016,1;
	monster "turbo_n_1",286,284,"ATIRADOR DO BOPE",1016,1;
	monster "turbo_n_1",286,264,"ATIRADOR DO BOPE",1016,1;
	monster "turbo_n_1",294,284,"ATIRADOR DO BOPE",1016,1;
	monster "turbo_n_1",294,264,"ATIRADOR DO BOPE",1016,1;
	monster "turbo_n_1",302,284,"ATIRADOR DO BOPE",1016,1;
	monster "turbo_n_1",302,264,"ATIRADOR DO BOPE",1016,1;
	monster "turbo_n_1",310,284,"ATIRADOR DO BOPE",1016,1;
	monster "turbo_n_1",310,264,"ATIRADOR DO BOPE",1016,1;
	monster "turbo_n_1",346,284,"ATIRADOR DO BOPE",1016,1;
	monster "turbo_n_1",346,264,"ATIRADOR DO BOPE",1016,1;
	monster "turbo_n_1",355,284,"ATIRADOR DO BOPE",1016,1;
	monster "turbo_n_1",355,264,"ATIRADOR DO BOPE",1016,1;
	monster "turbo_n_1",362,284,"ATIRADOR DO BOPE",1016,1;
	monster "turbo_n_1",362,264,"ATIRADOR DO BOPE",1016,1;
	monster "turbo_n_1",370,284,"ATIRADOR DO BOPE",1016,1;
	monster "turbo_n_1",370,264,"ATIRADOR DO BOPE",1016,1;
	monster "turbo_n_1",378,284,"ATIRADOR DO BOPE",1016,1;
	monster "turbo_n_1",378,264,"ATIRADOR DO BOPE",1016,1;

	$@CrazyRun = 1;
	$@CrazyRunEnter = 1;
	$@RunnerWin = 0;
	$@CheckRunner = 1;
	.time = .anuncio;
	for(.@i = 0; .@i < .time; .@i++) {
		if ($@CrazyRunEnter != 1) end;
		announce "[Corrida Maluca]: O evento ir� come�ar em "+.anuncio+" minuto"+((.anuncio == 1)?"":"s")+". Use @evento para participar!",8;
		.anuncio--;
		sleep (60 * 1000);
	}
	announce "[Corrida Maluca]: O evento come�ou!",8;
	$@CrazyRunEnter = 0;
	.anuncio = .time;
	donpcevent strnpcinfo(0)+"::OnBeginEvent";
	end;	
	
OnBeginEvent:
	sleep 5000;
	if (getmapusers(.mapa$) < 1) {
		announce "[Corrida Maluca]: Nenhum jogador compareceu ao evento, o evento foi finalizado.",0;
		$@CrazyRun = 0;
		end;
	}	
	.timer = gettimetick(2) + (60 * .tempoEvento);
	mapannounce .mapa$,"[Corrida Maluca]: Preparem-se! A corrida tem dura��o de "+.tempoEvento+" minutos.",bc_map,0;
	sleep 2000;
	mapannounce .mapa$,"[Corrida Maluca]: EM SUAS MARCAS...",bc_map,0;
	sleep 2000;
	mapannounce .mapa$,"[Corrida Maluca]: PREPARAR...",bc_map,0;
	sleep 2000;
	mapannounce .mapa$,"[Corrida Maluca]: GOGOGOGOGOGOOGOGOGOGO!!!!",bc_map,0;
	$@CheckRunner = 0;
		while($@CrazyRun){
			if(.timer < gettimetick(2) || getmapusers(.mapa$) == 0){
				announce "[Corrida Maluca]: O Evento chegou ao fim. Obrigado a todos que participaram!",8;
				sleep 5000;
				mapwarp .mapa$, "prontera", 156, 180;
				killmonsterall .mapa$;
				$@CrazyRun = 0;
				end;
			}
		sleep 10000;
		}	
	end;
}

turbo_n_1,328,274,0	script	Curandcorrida	HIDDEN_WARP_NPC,5,5,{
OnTouch:
	percentheal 100,100;
	//emotion 51;
	end;
}

turbo_n_1,63,350,5	script	StartCheck	-1,1,30,{
OnInit: 
	atcommand "@skilloff";
	end;
OnTouch:
	if ($@CheckRunner) {
		mes "^FF7F00[Corrida Maluca]^000000";
		mes "- ^FF0000TENTATIVA DE QUEIMAR A LARGADA DETECTADA^000000 -";
		sleep2 1000;
		warp "turbo_n_1",54,364;
		end;
	}
}

turbo_n_1,370,57,0	script	FinishCheck	-1,0,30,{
OnInit:
	$@RunnerWin = 0;
	end;
OnTouch:
		$@RunnerWin++;
if ($@RunnerWin == 1 ) {
			getitem 7539, 3;
			dispbottom "[EvolutionBR] - Voc� recebeu 3x "+getitemname(7539)+".";
			callsub OnLastLap, "venceu a", 1;
		}
		else if ($@RunnerWin == 2) { 
			getitem 7539, 2;
			dispbottom "[EvolutionBR] - Voc� recebeu 2x "+getitemname(7539)+".";
			callsub OnLastLap, "venceu a", 2;
		}
		else if ($@RunnerWin == 3) { 
			getitem 7539, 1;
			dispbottom "[EvolutionBR] - Voc� recebeu 1x "+getitemname(7539)+".";
			callsub OnLastLap, "venceu a", 3;
		}
OnLastLap:
	announce "[Corrida Maluca]: "+strcharinfo(0)+" "+getarg(0)+" Corrida Maluca!!", bc_all;
	if ($@RunnerWin == 3) {
		killmonsterall "turbo_n_1";
		$@RunnerWin = 0;	
		mapwarp "turbo_n_1", "prontera", 156, 180;
		end; 
	}
	warp "prontera", 156, 180;
	end;
return;
}
/* Portais */
turbo_n_1,169,364,0	warp	CMwarp1	1,1,turbo_n_1,210,368
turbo_n_1,233,367,0	warp	CMwarp3	1,1,turbo_n_1,316,365
turbo_n_1,385,366,0	warp	CMwarp4	1,1,turbo_n_1,11,267
turbo_n_1,114,191,0	warp	CMwarp5	1,1,turbo_n_1,251,200
turbo_n_1,217,212,0	warp	CMwarp6	1,1,turbo_n_1,268,275
turbo_n_1,389,275,0	warp	CMwarp7	1,1,turbo_n_1,4,91
turbo_n_1,96,19,0	warp	CMwarp8	1,1,turbo_n_1,176,10
turbo_n_1,223,65,0	warp	CMwarp9	1,1,turbo_n_1,306,46
/* Portais */

/* Mapflags */
turbo_n_1	mapflag	nowarp
turbo_n_1	mapflag	nowarpto
turbo_n_1	mapflag	noteleport
turbo_n_1	mapflag	monster_noteleport
turbo_n_1	mapflag	nosave
turbo_n_1	mapflag	nomemo
turbo_n_1	mapflag	nobranch
turbo_n_1	mapflag	nopenalty
turbo_n_1	mapflag	nomobloot
turbo_n_1	mapflag	nomvploot
turbo_n_1	mapflag	noicewall
/* MapFlags */

/* Inicio Armadilhas 01 */
turbo_n_1,118,377,0	script	trap_a#n_1-1::TurboLogRace_n_1	-1,44,1,{
OnTouch:
	switch(rand(1,3)) {
		case 1:
			warp "turbo_n_1",72,372;
			end;
		case 2:
			warp "turbo_n_1",72,365;
			end;
		case 3:
			warp "turbo_n_1",72,357;
			end;
	}
}
turbo_n_1,77,375,0	duplicate(TurboLogRace_n_1)	trap_a#n_1-2	-1,3,1
turbo_n_1,82,375,0	duplicate(TurboLogRace_n_1)	trap_a#n_1-3	-1,1,1
turbo_n_1,82,372,0	duplicate(TurboLogRace_n_1)	trap_a#n_1-4	-1,0,2
turbo_n_1,83,372,0	duplicate(TurboLogRace_n_1)	trap_a#n_1-5	-1,0,2
turbo_n_1,97,374,0	duplicate(TurboLogRace_n_1)	trap_a#n_1-6	-1,1,2
turbo_n_1,98,374,0	duplicate(TurboLogRace_n_1)	trap_a#n_1-7	-1,1,2
turbo_n_1,122,372,0	duplicate(TurboLogRace_n_1)	trap_a#n_1-8	-1,2,4
turbo_n_1,125,374,0	duplicate(TurboLogRace_n_1)	trap_a#n_1-9	-1,0,4
turbo_n_1,132,374,0	duplicate(TurboLogRace_n_1)	trap_a#n_1-10	-1,7,2
turbo_n_1,156,374,0	duplicate(TurboLogRace_n_1)	trap_a#n_1-11	-1,6,2
turbo_n_1,163,375,0	duplicate(TurboLogRace_n_1)	trap_a#n_1-12	-1,0,3
turbo_n_1,164,377,0	duplicate(TurboLogRace_n_1)	trap_a#n_1-13	-1,1,1
turbo_n_1,76,369,0	duplicate(TurboLogRace_n_1)	trap_b#n_1-1	-1,2,2
turbo_n_1,79,369,0	duplicate(TurboLogRace_n_1)	trap_b#n_1-2	-1,0,2
turbo_n_1,77,366,0	duplicate(TurboLogRace_n_1)	trap_b#n_1-3	-1,3,0
turbo_n_1,85,366,0	duplicate(TurboLogRace_n_1)	trap_b#n_1-4	-1,5,1
turbo_n_1,87,363,0	duplicate(TurboLogRace_n_1)	trap_b#n_1-5	-1,2,1
turbo_n_1,88,368,0	duplicate(TurboLogRace_n_1)	trap_b#n_1-6	-1,2,5
turbo_n_1,92,370,0	duplicate(TurboLogRace_n_1)	trap_b#n_1-7	-1,1,3
turbo_n_1,98,368,0	duplicate(TurboLogRace_n_1)	trap_b#n_1-8	-1,4,1
turbo_n_1,109,371,0	duplicate(TurboLogRace_n_1)	trap_b#n_1-9	-1,7,2
turbo_n_1,110,368,0	duplicate(TurboLogRace_n_1)	trap_b#n_1-10	-1,7,0
turbo_n_1,113,366,0	duplicate(TurboLogRace_n_1)	trap_b#n_1-11	-1,3,2
turbo_n_1,117,368,0	duplicate(TurboLogRace_n_1)	trap_b#n_1-12	-1,0,5
turbo_n_1,123,364,0	duplicate(TurboLogRace_n_1)	trap_b#n_1-13	-1,6,1
turbo_n_1,136,368,0	duplicate(TurboLogRace_n_1)	trap_b#n_1-14	-1,8,1
turbo_n_1,136,366,0	duplicate(TurboLogRace_n_1)	trap_b#n_1-15	-1,8,0
turbo_n_1,144,370,0	duplicate(TurboLogRace_n_1)	trap_b#n_1-16	-1,2,3
turbo_n_1,147,370,0	duplicate(TurboLogRace_n_1)	trap_b#n_1-17	-1,0,3
turbo_n_1,155,369,0	duplicate(TurboLogRace_n_1)	trap_b#n_1-18	-1,7,0
turbo_n_1,155,368,0	duplicate(TurboLogRace_n_1)	trap_b#n_1-19	-1,7,0
turbo_n_1,151,367,0	duplicate(TurboLogRace_n_1)	trap_b#n_1-20	-1,3,0
turbo_n_1,153,366,0	duplicate(TurboLogRace_n_1)	trap_b#n_1-21	-1,1,0
turbo_n_1,155,367,0	duplicate(TurboLogRace_n_1)	trap_b#n_1-22	-1,0,1
turbo_n_1,78,362,0	duplicate(TurboLogRace_n_1)	trap_c#n_1-1	-1,4,1
turbo_n_1,78,359,0	duplicate(TurboLogRace_n_1)	trap_c#n_1-2	-1,4,1
turbo_n_1,83,362,0	duplicate(TurboLogRace_n_1)	trap_c#n_1-3	-1,0,1
turbo_n_1,88,359,0	duplicate(TurboLogRace_n_1)	trap_c#n_1-4	-1,5,1
turbo_n_1,89,357,0	duplicate(TurboLogRace_n_1)	trap_c#n_1-5	-1,1,1
turbo_n_1,92,390,0	duplicate(TurboLogRace_n_1)	trap_c#n_1-6	-1,1,1
turbo_n_1,92,357,0	duplicate(TurboLogRace_n_1)	trap_c#n_1-7	-1,1,1
turbo_n_1,98,364,0	duplicate(TurboLogRace_n_1)	trap_c#n_1-8	-1,6,1
turbo_n_1,98,362,0	duplicate(TurboLogRace_n_1)	trap_c#n_1-9	-1,6,0
turbo_n_1,106,364,0	duplicate(TurboLogRace_n_1)	trap_c#n_1-10	-1,2,2
turbo_n_1,107,360,0	duplicate(TurboLogRace_n_1)	trap_c#n_1-11	-1,1,2
turbo_n_1,109,360,0	duplicate(TurboLogRace_n_1)	trap_c#n_1-12	-1,0,2
turbo_n_1,112,361,0	duplicate(TurboLogRace_n_1)	trap_c#n_1-13	-1,3,1
turbo_n_1,116,359,0	duplicate(TurboLogRace_n_1)	trap_c#n_1-14	-1,0,2
turbo_n_1,117,359,0	duplicate(TurboLogRace_n_1)	trap_c#n_1-15	-1,0,2
turbo_n_1,116,356,0	duplicate(TurboLogRace_n_1)	trap_c#n_1-16	-1,0,2
turbo_n_1,117,356,0	duplicate(TurboLogRace_n_1)	trap_c#n_1-17	-1,0,2
turbo_n_1,129,360,0	duplicate(TurboLogRace_n_1)	trap_c#n_1-18	-1,12,1
turbo_n_1,129,358,0	duplicate(TurboLogRace_n_1)	trap_c#n_1-19	-1,12,0
turbo_n_1,132,357,0	duplicate(TurboLogRace_n_1)	trap_c#n_1-20	-1,2,1
turbo_n_1,137,357,0	duplicate(TurboLogRace_n_1)	trap_c#n_1-21	-1,2,1
turbo_n_1,147,363,0	duplicate(TurboLogRace_n_1)	trap_c#n_1-22	-1,16,1
turbo_n_1,149,365,0	duplicate(TurboLogRace_n_1)	trap_c#n_1-23	-1,1,0
turbo_n_1,158,362,0	duplicate(TurboLogRace_n_1)	trap_c#n_1-24	-1,1,4
turbo_n_1,154,360,0	duplicate(TurboLogRace_n_1)	trap_c#n_1-25	-1,2,2
turbo_n_1,161,360,0	duplicate(TurboLogRace_n_1)	trap_c#n_1-26	-1,1,2
turbo_n_1,161,365,0	duplicate(TurboLogRace_n_1)	trap_c#n_1-27	-1,1,0
turbo_n_1,160,366,0	duplicate(TurboLogRace_n_1)	trap_c#n_1-28	-1
turbo_n_1,79,353,0	duplicate(TurboLogRace_n_1)	trap_d#n_1-1	-1,5,2
turbo_n_1,85,354,0	duplicate(TurboLogRace_n_1)	trap_d#n_1-2	-1,0,1
turbo_n_1,99,353,0	duplicate(TurboLogRace_n_1)	trap_d#n_1-3	-1,14,0
turbo_n_1,99,352,0	duplicate(TurboLogRace_n_1)	trap_d#n_1-4	-1,14,0
turbo_n_1,99,356,0	duplicate(TurboLogRace_n_1)	trap_d#n_1-5	-1,3,3
turbo_n_1,103,356,0	duplicate(TurboLogRace_n_1)	trap_d#n_1-6	-1,0,3
turbo_n_1,108,354,0	duplicate(TurboLogRace_n_1)	trap_d#n_1-7	-1,5,1
turbo_n_1,112,356,0	duplicate(TurboLogRace_n_1)	trap_d#n_1-8	-1,0,1
turbo_n_1,113,356,0	duplicate(TurboLogRace_n_1)	trap_d#n_1-9	-1,0,1
turbo_n_1,123,353,0	duplicate(TurboLogRace_n_1)	trap_d#n_1-10	-1,3,2
turbo_n_1,127,353,0	duplicate(TurboLogRace_n_1)	trap_d#n_1-11	-1,0,2
turbo_n_1,145,352,0	duplicate(TurboLogRace_n_1)	trap_d#n_1-12	-1,17,1
turbo_n_1,152,354,0	duplicate(TurboLogRace_n_1)	trap_d#n_1-13	-1,10,1
turbo_n_1,145,357,0	duplicate(TurboLogRace_n_1)	trap_d#n_1-14	-1,1,2
turbo_n_1,148,357,0	duplicate(TurboLogRace_n_1)	trap_d#n_1-15	-1,1,2
/* Fim Armadilhas 01 */

/* Inicio Armadilhas 02 */
turbo_n_1,322,354,0	script	#TBT_trap_n1-::TurboSandHill_n1	-1,3,3,{
OnTouch:
	switch(rand(1,9)) {
	case 1:
	case 9:
		sc_start SC_CONFUSION,8000,0;
		//emotion ET_PROFUSELY_SWEAT,1;
		end;
	case 2:
		sc_start SC_STONE,4000,0;
		end;
	case 4:
		sc_start SC_SLEEP,4000,0;
		end;
	case 6:
		sc_start SC_FREEZE,4000,0;
		//emotion ET_PROFUSELY_SWEAT,1;
		end;
	case 8:
		sc_start SC_STUN,4000,0;
		end;
	}
}
turbo_n_1,323,360,0	duplicate(TurboSandHill_n1)	#TBT_trap_n1-2	-1,3,3
turbo_n_1,324,365,0	duplicate(TurboSandHill_n1)	#TBT_trap_n1-3	-1,1,1
turbo_n_1,325,370,0	duplicate(TurboSandHill_n1)	#TBT_trap_n1-4	-1,3,3
turbo_n_1,325,375,0	duplicate(TurboSandHill_n1)	#TBT_trap_n1-5	-1,1,1
turbo_n_1,329,377,0	duplicate(TurboSandHill_n1)	#TBT_trap_n1-6	-1,3,3
turbo_n_1,338,372,0	duplicate(TurboSandHill_n1)	#TBT_trap_n1-7	-1,3,3
turbo_n_1,341,364,0	duplicate(TurboSandHill_n1)	#TBT_trap_n1-8	-1,3,3
turbo_n_1,325,359,0	duplicate(TurboSandHill_n1)	#TBT_trap_n1-9	-1,2,2
turbo_n_1,341,355,0	duplicate(TurboSandHill_n1)	#TBT_trap_n1-10	-1,1,1
turbo_n_1,350,355,0	duplicate(TurboSandHill_n1)	#TBT_trap_n1-11	-1,3,3
turbo_n_1,348,363,0	duplicate(TurboSandHill_n1)	#TBT_trap_n1-12	-1,1,1
turbo_n_1,347,370,0	duplicate(TurboSandHill_n1)	#TBT_trap_n1-13	-1,2,2
turbo_n_1,349,377,0	duplicate(TurboSandHill_n1)	#TBT_trap_n1-14	-1,9,9
turbo_n_1,362,372,0	duplicate(TurboSandHill_n1)	#TBT_trap_n1-15	-1,3,3
turbo_n_1,364,365,0	duplicate(TurboSandHill_n1)	#TBT_trap_n1-16	-1,1,1
turbo_n_1,363,357,0	duplicate(TurboSandHill_n1)	#TBT_trap_n1-17	-1,3,3
turbo_n_1,374,358,0	duplicate(TurboSandHill_n1)	#TBT_trap_n1-18	-1,2,2
turbo_n_1,371,367,0	duplicate(TurboSandHill_n1)	#TBT_trap_n1-19	-1,3,3
turbo_n_1,371,376,0	duplicate(TurboSandHill_n1)	#TBT_trap_n1-20	-1,1,1
turbo_n_1,379,375,0	duplicate(TurboSandHill_n1)	#TBT_trap_n1-21	-1,3,3
turbo_n_1,382,363,0	duplicate(TurboSandHill_n1)	#TBT_trap_n1-22	-1,2,2
turbo_n_1,381,354,0	duplicate(TurboSandHill_n1)	#TBT_trap_n1-23	-1,1,1
/* Fim Armadilhas 02 */

/* Inicio Armadilhas 03 */
turbo_n_1,355,362,0	script	#TBT_trap_n1-24	-1,3,3,{
OnTouch:
	if (rand(1,3) == 1) {
		cutin "kafra_03",2;
		mes "^4d4dffO Turbo Track foi";
		mes "trazido para Al de Baran";
		mes " e para voc� pela";
		mes "^800000Corpora��o Kafra^4DD4DF.^000000";
		next;
		mes "^4d4dffN�s desejamos boa sorte para todos";
		mes "os participantes da Turbo Track de hoje";
		mes "e agrade�o de todo cora��o a todos por usar os Servi�os Kafra.^000000";
		next;
		mes "^800000A Corpora��o Kafra^4d4dff tem promovido os servi�os de Armaz�m,";
		mes "Ponto de Retorno e Teleportes^4d4dff para nossos clientes h� anos. Apenas escute esses reais clientes...^000000";
		next;
		mes "^4d4dff[Karkatan]";
		mes "Minha terra sofria de servi�os pobres para os clientes... at� a Kafra chegar!";
		mes " ";
		mes "[Curador Guiss]";
		mes "Oh, A Kafra � simplesmente a melhor!^000000";
		next;
		mes "^ff0000Turbo Track";
		mes "^ff0000Armadilhas no Percurso do Deserto!";
		mes "^4d4dffPatrocinado pela ^800000Corpora��o Kafra^4d4dff";
		mes "''N�s estamos sempre do seu lado.''^000000";
		close2;
		cutin "kafra_03",255;
		end;
	}
	else {
		sc_start SC_CONFUSION,4000,0;
		end;
	}
}
/* Fim Armadilhas 03 */

/* Inicio Armadilhas 04 */
turbo_n_1,13,266,0	script	flasher#n_1-01#turbo::TurboWaterMaze_n_1	-1,0,2,{
OnTouch:
	sc_start SC_BLIND,60000,0;
	end;
}
turbo_n_1,24,268,0	duplicate(TurboWaterMaze_n_1)	flasher#n_1-02	-1,1,0
turbo_n_1,20,258,0	duplicate(TurboWaterMaze_n_1)	flasher#n_1-03	-1,1,0
turbo_n_1,23,251,0	duplicate(TurboWaterMaze_n_1)	flasher#n_1-04	-1,0,1
turbo_n_1,36,270,0	duplicate(TurboWaterMaze_n_1)	flasher#n_1-05	-1,1,0
turbo_n_1,22,239,0	duplicate(TurboWaterMaze_n_1)	flasher#n_1-06	-1,1,0
turbo_n_1,38,239,0	duplicate(TurboWaterMaze_n_1)	flasher#n_1-07	-1,0,1
turbo_n_1,37,237,0	duplicate(TurboWaterMaze_n_1)	flasher#n_1-08	-1,1,0
turbo_n_1,55,247,0	duplicate(TurboWaterMaze_n_1)	flasher#n_1-09	-1,0,1
turbo_n_1,55,246,0	duplicate(TurboWaterMaze_n_1)	flasher#n_1-10	-1,1,0
turbo_n_1,63,253,0	duplicate(TurboWaterMaze_n_1)	flasher#n_1-11	-1,0,1
turbo_n_1,36,216,0	duplicate(TurboWaterMaze_n_1)	flasher#n_1-12	-1,1,0
turbo_n_1,20,209,0	duplicate(TurboWaterMaze_n_1)	flasher#n_1-13	-1,1,0
turbo_n_1,28,195,0	duplicate(TurboWaterMaze_n_1)	flasher#n_1-14	-1,1,0
turbo_n_1,82,264,0	duplicate(TurboWaterMaze_n_1)	flasher#n_1-15	-1,1,0
turbo_n_1,47,185,0	duplicate(TurboWaterMaze_n_1)	flasher#n_1-16	-1,0,1
turbo_n_1,53,207,0	duplicate(TurboWaterMaze_n_1)	flasher#n_1-17	-1,1,0
turbo_n_1,54,208,0	duplicate(TurboWaterMaze_n_1)	flasher#n_1-18	-1,0,1
turbo_n_1,81,247,0	duplicate(TurboWaterMaze_n_1)	flasher#n_1-19	-1,0,1
turbo_n_1,105,257,0	duplicate(TurboWaterMaze_n_1)	flasher#n_1-20	-1,0,1
turbo_n_1,95,242,0	duplicate(TurboWaterMaze_n_1)	flasher#n_1-21	-1,0,1
turbo_n_1,77,232,0	duplicate(TurboWaterMaze_n_1)	flasher#n_1-22	-1,2,0
turbo_n_1,67,222,0	duplicate(TurboWaterMaze_n_1)	flasher#n_1-23	-1,2,0
turbo_n_1,83,206,0	duplicate(TurboWaterMaze_n_1)	flasher#n_1-24	-1,0,1
turbo_n_1,95,224,0	duplicate(TurboWaterMaze_n_1)	flasher#n_1-25	-1,0,1
turbo_n_1,106,220,0	duplicate(TurboWaterMaze_n_1)	flasher#n_1-26	-1,2,0
turbo_n_1,93,191,0	duplicate(TurboWaterMaze_n_1)	flasher#n_1-27	-1,1,0
turbo_n_1,94,192,0	duplicate(TurboWaterMaze_n_1)	flasher#n_1-28a	-1,0,1
turbo_n_1,46,214,0	duplicate(TurboWaterMaze_n_1)	flasher#n_1-28b	-1,0,1
turbo_n_1,16,247,0	duplicate(TurboWaterMaze_n_1)	flasher#n_1-29	-1,1,0
turbo_n_1,58,268,0	duplicate(TurboWaterMaze_n_1)	flasher#n_1-30	-1,1,1
turbo_n_1,36,253,0	duplicate(TurboWaterMaze_n_1)	flasher#n_1-31	-1,1,0
turbo_n_1,69,238,0	duplicate(TurboWaterMaze_n_1)	flasher#n_1-32	-1,1,1
turbo_n_1,58,268,0	duplicate(TurboWaterMaze_n_1)	flasher#n_1-33	-1,1,1
turbo_n_1,74,188,0	duplicate(TurboWaterMaze_n_1)	flasher#n_1-34	-1,2,0
turbo_n_1,99,207,0	duplicate(TurboWaterMaze_n_1)	flasher#n_1-35	-1,1,1
turbo_n_1,74,188,0	duplicate(TurboWaterMaze_n_1)	flasher#n_1-36	-1,2,0
turbo_n_1,111,188,0	duplicate(TurboWaterMaze_n_1)	flasher#n_1-37a	-1,1,0
turbo_n_1,51,232,0	duplicate(TurboWaterMaze_n_1)	flasher#n_1-37b	-1,1,1
turbo_n_1,30,232,0	duplicate(TurboWaterMaze_n_1)	flasher#n_1-38	-1,1,1
turbo_n_1,92,256,0	duplicate(TurboWaterMaze_n_1)	flasher#n_1-39	-1,1,1
turbo_n_1,79,220,0	duplicate(TurboWaterMaze_n_1)	flasher#n_1-40	-1,1,1
turbo_n_1,51,192,0	duplicate(TurboWaterMaze_n_1)	flasher#n_1-41	-1,1,1
turbo_n_1,22,227,0	duplicate(TurboWaterMaze_n_1)	flasher#n_1-42	-1,1,1
turbo_n_1,51,232,0	duplicate(TurboWaterMaze_n_1)	flasher#n_1-43	-1,1,1
turbo_n_1,42,258,0	duplicate(TurboWaterMaze_n_1)	flasher#n_1-44	-1,1,0
turbo_n_1,45,271,0	duplicate(TurboWaterMaze_n_1)	flasher#n_1-45	-1,2,1
turbo_n_1,72,207,0	duplicate(TurboWaterMaze_n_1)	flasher#n_1-46	-1,1,1
turbo_n_1,33,192,0	duplicate(TurboWaterMaze_n_1)	flasher#n_1-47	-1,0,1
turbo_n_1,90,241,0	duplicate(TurboWaterMaze_n_1)	flasher#n_1-48a	-1,1,0
turbo_n_1,236,3,0	duplicate(TurboWaterMaze_n_1)	flasher#n_1-48b	-1,3,0
/* Fim Armadilhas 04 */

/* Inicio Armadilhas 05 */
turbo_n_1,324,279,0	script	snake01#n1	WARPNPC,1,1,{
OnTouch:
	.@turbo2 = rand(1,7);
	if (.@turbo2 > 0) && (.@turbo2 < 3)
		warp "turbo_n_1",370,292;
	else if (.@turbo2 > 2) && (.@turbo2 < 5)
		warp "turbo_n_1",295,293;
	else if (.@turbo2 > 4) && (.@turbo2 < 7)
		warp "turbo_n_1",355,292;
	else
		warp "turbo_n_1",279,292;
	end;
}
turbo_n_1,332,279,0	script	snake02#n1	WARPNPC,1,1,{
OnTouch:
	.@turbo2 = rand(1,8);
	if (.@turbo2 > 0) && (.@turbo2 < 3)
		warp "turbo_n_1",287,256;
	else if (.@turbo2 > 2) && (.@turbo2 < 5)
		warp "turbo_n_1",303,256;
	else if (.@turbo2 > 4) && (.@turbo2 < 7)
		warp "turbo_n_1",347,256;
	else
		warp "turbo_n_1",363,256;
	end;
}
turbo_n_1,324,270,0	script	snake03#n1	WARPNPC,1,1,{
OnTouch:
	.@turbo2 = rand(1,8);
	if (.@turbo2 > 0) && (.@turbo2 < 3)
		warp "turbo_n_1",279,292;
	else if (.@turbo2 > 2) && (.@turbo2 < 5)
		warp "turbo_n_1",311,292;
	else if (.@turbo2 > 4) && (.@turbo2 < 7)
		warp "turbo_n_1",347,256;
	else
		warp "turbo_n_1",370,292;
	end;
}
turbo_n_1,332,270,0	script	snake04#n1	WARPNPC,1,1,{
OnTouch:
	.@turbo2 = rand(1,7);
	if (.@turbo2 > 0) && (.@turbo2 < 3)
		warp "turbo_n_1",363,256;
	else if (.@turbo2 > 2) && (.@turbo2 < 5)
		warp "turbo_n_1",295,293;
	else if (.@turbo2 > 4) && (.@turbo2 < 7)
		warp "turbo_n_1",355,292;
	else
		warp "turbo_n_1",287,256;
	end;
}
/* Fim Armadilhas 05 */

/* Inicio Armadilhas 06 */
turbo_n_1,179,55,0	script	#n1Intersectiona::TurboHint_n1_1	-1,1,1,{
OnTouch:
	//emotion ET_QUESTION,1;
	end;
}
turbo_n_1,184,45,0	duplicate(TurboHint_n1_1)	#n1Intersectionb	-1,1,1
turbo_n_1,181,30,0	duplicate(TurboHint_n1_1)	#n1Intersectionc	-1,1,1
turbo_n_1,186,28,0	duplicate(TurboHint_n1_1)	#n1Intersectiond	-1,1,1
turbo_n_1,183,19,0	duplicate(TurboHint_n1_1)	#n1Intersectione	-1,1,1
turbo_n_1,191,37,0	duplicate(TurboHint_n1_1)	#n1Intersectionf	-1,1,1
turbo_n_1,173,25,0	duplicate(TurboHint_n1_1)	#n1Intersectiong	-1,1,1
turbo_n_1,201,21,0	duplicate(TurboHint_n1_1)	#n1Intersectionh	-1,1,1
turbo_n_1,222,21,0	duplicate(TurboHint_n1_1)	#n1Intersectioni	-1,1,1
turbo_n_1,214,39,0	duplicate(TurboHint_n1_1)	#n1Intersectionj	-1,1,1
turbo_n_1,222,48,0	duplicate(TurboHint_n1_1)	#n1Intersectionk	-1,1,1
turbo_n_1,214,60,0	duplicate(TurboHint_n1_1)	#n1Intersectionl	-1,1,1
turbo_n_1,209,61,0	duplicate(TurboHint_n1_1)	#n1Intersectionm	-1,1,1
turbo_n_1,208,56,0	duplicate(TurboHint_n1_1)	#n1Intersectionn	-1,1,1
/* Fim Armadilhas 06 */

/* Inicio Armadilhas 07 */
turbo_n_1,178,63,0	script	#n1CorrectPath1::TurboHint_n1_2	-1,1,1,{
OnTouch:
	//emotion ET_SURPRISE,1;
	end;
}
turbo_n_1,182,40,0	duplicate(TurboHint_n1_2)	#n1CorrectPath2	-1,1,1
turbo_n_1,176,29,0	duplicate(TurboHint_n1_2)	#n1CorrectPath3	-1,1,1
turbo_n_1,180,25,0	duplicate(TurboHint_n1_2)	#n1CorrectPath4	-1,1,1
turbo_n_1,201,157,0	duplicate(TurboHint_n1_2)	#n1CorrectPath5	-1,1,1
turbo_n_1,203,65,0	duplicate(TurboHint_n1_2)	#n1CorrectPath6	-1,1,1
turbo_n_1,208,52,0	duplicate(TurboHint_n1_2)	#n1CorrectPath7	-1,1,1
turbo_n_1,208,26,0	duplicate(TurboHint_n1_2)	#n1CorrectPath8	-1,1,1
turbo_n_1,191,30,0	duplicate(TurboHint_n1_2)	#n1CorrectPath9	-1,1,1
/* Fim Armadilhas 07 */

/* Inicio Armadilhas 08 */
turbo_n_1,187,55,0	script	#n1NoWayOut1::TurboHint_n1_3	-1,1,1,{
OnTouch:
	//emotion ET_PROFUSELY_SWEAT,1;
	if (rand(1,4) == 3)
		//emotion ET_SURPRISE;
	end;
}
turbo_n_1,176,45,0	duplicate(TurboHint_n1_3)	#n1NoWayOut2	-1,1,1
turbo_n_1,222,26,0	duplicate(TurboHint_n1_3)	#n1NoWayOut3	-1,1,1
turbo_n_1,219,39,0	duplicate(TurboHint_n1_3)	#n1NoWayOut4	-1,1,1
turbo_n_1,222,61,0	duplicate(TurboHint_n1_3)	#n1NoWayOut6	-1,1,1
turbo_n_1,222,65,0	duplicate(TurboHint_n1_3)	#n1NoWayOut7	-1,1,1
/* Fim Armadilhas 08 */

/* Inicio Armadilhas 09 */
turbo_n_1,227,379,0	script	warp#n_1_1::warp_n_1	-1,1,1,{
OnTouch:
	.@warprand = rand(1,20);
	if (.@warprand == 1)
		warp "turbo_n_1",216,378;
	else if (.@warprand == 2)
		warp "turbo_n_1",218,360;
	else if (.@warprand == 4)
		warp "turbo_n_1",223,361;
	else if (.@warprand == 5)
		warp "turbo_n_1",243,342;
	else if (.@warprand == 6)
		warp "turbo_n_1",247,364;
	end;
}
turbo_n_1,237,380,0	duplicate(warp_n_1)	warp#n_1_2	-1,1,1,{
turbo_n_1,227,367,0	duplicate(warp_n_1)	warp#n_1_3	-1,1,1,{
turbo_n_1,231,360,0	duplicate(warp_n_1)	warp#n_1_4	-1,1,1,{
turbo_n_1,225,349,0	duplicate(warp_n_1)	warp#n_1_5	-1,1,1,{
turbo_n_1,249,352,0	duplicate(warp_n_1)	warp#n_1_6	-1,1,1,{
turbo_n_1,253,364,0	duplicate(warp_n_1)	warp#n_1_7	-1,2,2,{
/* Fim Armadilhas 09 */

/* Inicio Armadilhas 10 */
turbo_n_1,307,55,0	script	trap_n1#F1::TurboTrap_n1	HIDDEN_WARP_NPC,1,1,{
OnTouch:
	specialeffect EF_BLASTMINEBOMB;
	.@HitTrap = 10;
	if (.@HitTrap > 0 && .@HitTrap < 4)
		percentheal -1,0;
	else if (.@HitTrap > 4 && .@HitTrap < 8)
		percentheal -5,0;
	else
		percentheal -2,0;
	end;
}
turbo_n_1,307,51,0	duplicate(TurboTrap_n1)	trap_n1#F2	HIDDEN_WARP_NPC,1,1
turbo_n_1,307,47,0	duplicate(TurboTrap_n1)	trap_n1#F3	HIDDEN_WARP_NPC,1,1
turbo_n_1,307,43,0	duplicate(TurboTrap_n1)	trap_n1#F4	HIDDEN_WARP_NPC,1,1
turbo_n_1,307,39,0	duplicate(TurboTrap_n1)	trap_n1#F5	HIDDEN_WARP_NPC,1,1
turbo_n_1,307,39,0	duplicate(TurboTrap_n1)	trap_n1#F96	HIDDEN_WARP_NPC,1,1
turbo_n_1,312,56,0	duplicate(TurboTrap_n1)	trap_n1#F6	HIDDEN_WARP_NPC,1,1
turbo_n_1,312,52,0	duplicate(TurboTrap_n1)	trap_n1#F7	HIDDEN_WARP_NPC,1,1
turbo_n_1,312,48,0	duplicate(TurboTrap_n1)	trap_n1#F8	HIDDEN_WARP_NPC,1,1
turbo_n_1,312,44,0	duplicate(TurboTrap_n1)	trap_n1#F9	HIDDEN_WARP_NPC,1,1
turbo_n_1,312,40,0	duplicate(TurboTrap_n1)	trap_n1#F10	HIDDEN_WARP_NPC,1,1
turbo_n_1,312,36,0	duplicate(TurboTrap_n1)	trap_n1#F11	HIDDEN_WARP_NPC,1,1
turbo_n_1,316,55,0	duplicate(TurboTrap_n1)	trap_n1#F12	HIDDEN_WARP_NPC,1,1
turbo_n_1,316,51,0	duplicate(TurboTrap_n1)	trap_n1#F13	HIDDEN_WARP_NPC,1,1
turbo_n_1,316,47,0	duplicate(TurboTrap_n1)	trap_n1#F14	HIDDEN_WARP_NPC,1,1
turbo_n_1,316,43,0	duplicate(TurboTrap_n1)	trap_n1#F15	HIDDEN_WARP_NPC,1,1
turbo_n_1,316,39,0	duplicate(TurboTrap_n1)	trap_n1#F16	HIDDEN_WARP_NPC,1,1
turbo_n_1,316,36,0	duplicate(TurboTrap_n1)	trap_n1#F17	HIDDEN_WARP_NPC,1,1
turbo_n_1,320,56,0	duplicate(TurboTrap_n1)	trap_n1#F18	HIDDEN_WARP_NPC,1,1
turbo_n_1,320,52,0	duplicate(TurboTrap_n1)	trap_n1#F19	HIDDEN_WARP_NPC,1,1
turbo_n_1,320,48,0	duplicate(TurboTrap_n1)	trap_n1#F20	HIDDEN_WARP_NPC,1,1
turbo_n_1,320,44,0	duplicate(TurboTrap_n1)	trap_n1#F21	HIDDEN_WARP_NPC,1,1
turbo_n_1,320,40,0	duplicate(TurboTrap_n1)	trap_n1#F22	HIDDEN_WARP_NPC,1,1
turbo_n_1,320,36,0	duplicate(TurboTrap_n1)	trap_n1#F23	HIDDEN_WARP_NPC,1,1
turbo_n_1,324,55,0	duplicate(TurboTrap_n1)	trap_n1#F24	HIDDEN_WARP_NPC,1,1
turbo_n_1,324,51,0	duplicate(TurboTrap_n1)	trap_n1#F25	HIDDEN_WARP_NPC,1,1
turbo_n_1,324,47,0	duplicate(TurboTrap_n1)	trap_n1#F26	HIDDEN_WARP_NPC,1,1
turbo_n_1,324,43,0	duplicate(TurboTrap_n1)	trap_n1#F27	HIDDEN_WARP_NPC,1,1
turbo_n_1,324,39,0	duplicate(TurboTrap_n1)	trap_n1#F28	HIDDEN_WARP_NPC,1,1
turbo_n_1,324,36,0	duplicate(TurboTrap_n1)	trap_n1#F29	HIDDEN_WARP_NPC,1,1
turbo_n_1,328,56,0	duplicate(TurboTrap_n1)	trap_n1#F30	HIDDEN_WARP_NPC,1,1
turbo_n_1,328,52,0	duplicate(TurboTrap_n1)	trap_n1#F31	HIDDEN_WARP_NPC,1,1
turbo_n_1,328,48,0	duplicate(TurboTrap_n1)	trap_n1#F32	HIDDEN_WARP_NPC,1,1
turbo_n_1,328,44,0	duplicate(TurboTrap_n1)	trap_n1#F33	HIDDEN_WARP_NPC,1,1
turbo_n_1,328,40,0	duplicate(TurboTrap_n1)	trap_n1#F34	HIDDEN_WARP_NPC,1,1
turbo_n_1,328,36,0	duplicate(TurboTrap_n1)	trap_n1#F35	HIDDEN_WARP_NPC,1,1
turbo_n_1,332,55,0	duplicate(TurboTrap_n1)	trap_n1#F36	HIDDEN_WARP_NPC,1,1
turbo_n_1,332,51,0	duplicate(TurboTrap_n1)	trap_n1#F37	HIDDEN_WARP_NPC,1,1
turbo_n_1,332,47,0	duplicate(TurboTrap_n1)	trap_n1#F38	HIDDEN_WARP_NPC,1,1
turbo_n_1,332,43,0	duplicate(TurboTrap_n1)	trap_n1#F39	HIDDEN_WARP_NPC,1,1
turbo_n_1,332,39,0	duplicate(TurboTrap_n1)	trap_n1#F40	HIDDEN_WARP_NPC,1,1
turbo_n_1,332,36,0	duplicate(TurboTrap_n1)	trap_n1#F41	HIDDEN_WARP_NPC,1,1
/* Fim Armadilhas 10 */

/* Inicio Armadilhas 11 */
turbo_n_1,336,56,0	script	trap_n1#F42::TurboTrap_n1_2	-1,1,1,{
OnTouch:
	specialeffect EF_FREEZING;
	.@HitTrap = 10;
	if (.@HitTrap > 0 && .@HitTrap < 4)
		percentheal -1,0;
	else if (.@HitTrap > 4 && .@HitTrap < 8) {
		percentheal -5,0;
		sc_start SC_FREEZE,3000,0;
	}
	else {
		sc_start SC_FREEZE,4000,0;
		percentheal -2,0;
	}
}
turbo_n_1,336,52,0	duplicate(TurboTrap_n1_2)	trap_n1#F43	HIDDEN_WARP_NPC,1,1
turbo_n_1,336,48,0	duplicate(TurboTrap_n1_2)	trap_n1#F44	HIDDEN_WARP_NPC,1,1
turbo_n_1,336,44,0	duplicate(TurboTrap_n1_2)	trap_n1#F45	HIDDEN_WARP_NPC,1,1
turbo_n_1,336,50,0	duplicate(TurboTrap_n1_2)	trap_n1#F46	HIDDEN_WARP_NPC,1,1
turbo_n_1,336,36,0	duplicate(TurboTrap_n1_2)	trap_n1#F47	HIDDEN_WARP_NPC,1,1
turbo_n_1,340,55,0	duplicate(TurboTrap_n1_2)	trap_n1#F48	HIDDEN_WARP_NPC,1,1
turbo_n_1,340,51,0	duplicate(TurboTrap_n1)	trap_n1#F49	HIDDEN_WARP_NPC,1,1
turbo_n_1,340,47,0	duplicate(TurboTrap_n1)	trap_n1#F50	HIDDEN_WARP_NPC,1,1
turbo_n_1,340,43,0	duplicate(TurboTrap_n1)	trap_n1#F51	HIDDEN_WARP_NPC,1,1
turbo_n_1,340,39,0	duplicate(TurboTrap_n1)	trap_n1#F52	HIDDEN_WARP_NPC,1,1
turbo_n_1,340,36,0	duplicate(TurboTrap_n1)	trap_n1#F53	HIDDEN_WARP_NPC,1,1
turbo_n_1,344,56,0	duplicate(TurboTrap_n1)	trap_n1#F54	HIDDEN_WARP_NPC,1,1
turbo_n_1,344,52,0	duplicate(TurboTrap_n1)	trap_n1#F55	HIDDEN_WARP_NPC,1,1
turbo_n_1,344,48,0	duplicate(TurboTrap_n1)	trap_n1#F56	HIDDEN_WARP_NPC,1,1
turbo_n_1,344,44,0	duplicate(TurboTrap_n1)	trap_n1#F57	HIDDEN_WARP_NPC,1,1
turbo_n_1,344,40,0	duplicate(TurboTrap_n1)	trap_n1#F58	HIDDEN_WARP_NPC,1,1
turbo_n_1,344,36,0	duplicate(TurboTrap_n1)	trap_n1#F59	HIDDEN_WARP_NPC,1,1
turbo_n_1,348,55,0	duplicate(TurboTrap_n1)	trap_n1#F60	HIDDEN_WARP_NPC,1,1
turbo_n_1,348,51,0	duplicate(TurboTrap_n1)	trap_n1#F61	HIDDEN_WARP_NPC,1,1
turbo_n_1,348,47,0	duplicate(TurboTrap_n1)	trap_n1#F62	HIDDEN_WARP_NPC,1,1
turbo_n_1,348,43,0	duplicate(TurboTrap_n1)	trap_n1#F63	HIDDEN_WARP_NPC,1,1
turbo_n_1,348,39,0	duplicate(TurboTrap_n1)	trap_n1#F64	HIDDEN_WARP_NPC,1,1
turbo_n_1,348,36,0	duplicate(TurboTrap_n1)	trap_n1#F65	HIDDEN_WARP_NPC,1,1
turbo_n_1,352,56,0	duplicate(TurboTrap_n1)	trap_n1#F66	HIDDEN_WARP_NPC,1,1
turbo_n_1,352,52,0	duplicate(TurboTrap_n1)	trap_n1#F67	HIDDEN_WARP_NPC,1,1
turbo_n_1,352,48,0	duplicate(TurboTrap_n1)	trap_n1#F68	HIDDEN_WARP_NPC,1,1
turbo_n_1,352,44,0	duplicate(TurboTrap_n1)	trap_n1#F69	HIDDEN_WARP_NPC,1,1
turbo_n_1,352,40,0	duplicate(TurboTrap_n1)	trap_n1#F70	HIDDEN_WARP_NPC,1,1
turbo_n_1,352,36,0	duplicate(TurboTrap_n1)	trap_n1#F71	HIDDEN_WARP_NPC,1,1
turbo_n_1,356,55,0	duplicate(TurboTrap_n1)	trap_n1#F72	HIDDEN_WARP_NPC,1,1
turbo_n_1,356,51,0	duplicate(TurboTrap_n1)	trap_n1#F73	HIDDEN_WARP_NPC,1,1
turbo_n_1,356,47,0	duplicate(TurboTrap_n1)	trap_n1#F74	HIDDEN_WARP_NPC,1,1
turbo_n_1,356,43,0	duplicate(TurboTrap_n1)	trap_n1#F75	HIDDEN_WARP_NPC,1,1
turbo_n_1,356,39,0	duplicate(TurboTrap_n1)	trap_n1#F76	HIDDEN_WARP_NPC,1,1
turbo_n_1,356,36,0	duplicate(TurboTrap_n1)	trap_n1#F77	HIDDEN_WARP_NPC,1,1
turbo_n_1,360,56,0	duplicate(TurboTrap_n1)	trap_n1#F78	HIDDEN_WARP_NPC,1,1
turbo_n_1,360,52,0	duplicate(TurboTrap_n1)	trap_n1#F79	HIDDEN_WARP_NPC,1,1
turbo_n_1,360,48,0	duplicate(TurboTrap_n1)	trap_n1#F80	HIDDEN_WARP_NPC,1,1
turbo_n_1,360,44,0	duplicate(TurboTrap_n1)	trap_n1#F81	HIDDEN_WARP_NPC,1,1
turbo_n_1,360,40,0	duplicate(TurboTrap_n1)	trap_n1#F82	HIDDEN_WARP_NPC,1,1
turbo_n_1,360,36,0	duplicate(TurboTrap_n1)	trap_n1#F83	HIDDEN_WARP_NPC,1,1
turbo_n_1,364,55,0	duplicate(TurboTrap_n1)	trap_n1#F84	HIDDEN_WARP_NPC,1,1
turbo_n_1,364,51,0	duplicate(TurboTrap_n1)	trap_n1#F85	HIDDEN_WARP_NPC,1,1
turbo_n_1,364,47,0	duplicate(TurboTrap_n1)	trap_n1#F86	HIDDEN_WARP_NPC,1,1
turbo_n_1,364,43,0	duplicate(TurboTrap_n1)	trap_n1#F87	HIDDEN_WARP_NPC,1,1
turbo_n_1,364,39,0	duplicate(TurboTrap_n1)	trap_n1#F88	HIDDEN_WARP_NPC,1,1
turbo_n_1,364,36,0	duplicate(TurboTrap_n1)	trap_n1#F89	HIDDEN_WARP_NPC,1,1
turbo_n_1,368,56,0	duplicate(TurboTrap_n1)	trap_n1#F90	HIDDEN_WARP_NPC,1,1
turbo_n_1,368,52,0	duplicate(TurboTrap_n1)	trap_n1#F91	HIDDEN_WARP_NPC,1,1
turbo_n_1,368,48,0	duplicate(TurboTrap_n1)	trap_n1#F92	HIDDEN_WARP_NPC,1,1
turbo_n_1,368,44,0	duplicate(TurboTrap_n1)	trap_n1#F93	HIDDEN_WARP_NPC,1,1
turbo_n_1,368,40,0	duplicate(TurboTrap_n1)	trap_n1#F94	HIDDEN_WARP_NPC,1,1
turbo_n_1,368,36,0	duplicate(TurboTrap_n1)	trap_n1#F95	HIDDEN_WARP_NPC,1,1
/* Fim Armadilhas 11 */

//Mapflag
turbo_n_1	mapflag	nocommand	99