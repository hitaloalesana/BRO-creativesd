-	script	tarot_controller	-1,{

OnInit:

	setarray $@mapeventtarot$, "poring_w01", 102, 71;		// = Configura��o do mapa do evento
	$@mintarot_participantes = 1;							// = N�mero m�nimo de participantes para iniciar o evento
	setarray $@premiacaoITEM, 607, 608, 609;				// = Itens
	setarray $@premiacaoQUANT, 1, 10, 20;					// = Quantidade de cada item
	callfunc ("resetEventoTarot", $@tarot_participantes$, $@mapeventtarot$[0]);
	setarray .mapflag,0,1,7,8,13,22,33,34,12;
	for (.@m = 0; .@m < getarraysize(.mapflag); .@m++){
		setmapflag $@mapeventtarot$[0], .mapflag[.@m];
	}
	end;
	
OnStartEvent:
	$tarot_status = 1;
	for(.@t = 3; .@t > 0; .@t--){
	
		// = Se vari�vel 0 ou GDE ativa, ent�o encerra.
		if ( !$tarot_status || agitcheck() || agitcheck2() ){
			callfunc ("resetEventoTarot", $@tarot_participantes$, $@mapeventtarot$[0]);
			end;
		}
		announce "[Evento Tar�]: O evento iniciar� em "+(.@t*20)+" segundos!",bc_all;
		sleep 20000;
	}
	
	if ( getarraysize($@tarot_participantes$) < $@mintarot_participantes ){
		announce "["+strnpcinfo(1)+"] O m�nimo de participantes requeridos n�o foi satisfeito!",bc_all;
		callfunc ("resetEventoTarot", $@tarot_participantes$, $@mapeventtarot$[0]);
		end;
	}

	announce "[Evento Tar�]: Come�ando!",bc_all;	
	$tarot_status = 0;
	for(.@p = 0; .@p < getarraysize($@tarot_participantes$); .@p++){
	
		if ( isloggedin(getcharid(3, $@tarot_participantes$[.@p])) ){
			warpchar $@mapeventtarot$[0], atoi($@mapeventtarot$[1]), atoi($@mapeventtarot$[2]), getcharid(0, $@tarot_participantes$[.@p]);
		}
		else {
			deletearray $@tarot_participantes$[.@p], 1;
		}
		
	}
	sleep 8000;
	donpcevent "[GM] Cartomante::OnSpeak";
	end;

	OnReturn:
	while( getarraysize($@tarot_participantes$) > 1){ // = Enquanto o total de participantes for maior que 1
		sleep 2000;
		for(.@p = 0; .@p < getarraysize($@tarot_participantes$); .@p++){
			attachrid(getcharid(3,$@tarot_participantes$[.@p]));
				if( strcharinfo(3) == $@mapeventtarot$[0] ){
					.@CARDs = 522+rand(1,14);
					specialeffect2 .@CARDs, strcharinfo(0);
					if( .@CARDs == 531 ){
						deletearray $@tarot_participantes$[.@p], 1;
						percentheal -99,-99;
						sleep2 1500;
						@randtarot = rand(1,3);
						#CASHPOINTS += @randtarot;
						dispbottom "Voc� recebeu ["+@randtarot+"] CASHPOINTS por participar do evento (Tarot).";
						warp "SavePoint",0,0;
					}
				}
				detachrid;
		}
	}
	sleep 2000;
	if ( getarraysize($@tarot_participantes$)){
		attachrid(getcharid(3, $@tarot_participantes$[0]));	// = Atacha o account_id apartir do nome
		if ( strcharinfo(3) == $@mapeventtarot$[0] ){
			announce "[Evento Tar�]: Temos um vencedor: ["+$@tarot_participantes$[0]+"]",bc_all;
			for(.@w = 0; .@w < getarraysize($@premiacaoITEM); .@w++){
				.@item = $@premiacaoITEM[.@w];
				.@quantidade = $@premiacaoQUANT[.@w];
				getitem .@item, .@quantidade;
				message strcharinfo(0), "Voc� recebeu: "+.@quantidade+"x "+getitemname(.@item);
			}
		}
		detachrid;
	}
	callfunc ("resetEventoTarot", $@tarot_participantes$, $@mapeventtarot$[0]);
	end;
}


-	script	tarot_events	-1,{
	OnPCDieEvent:
	OnPCLogoutEvent:
	if ( strcharinfo(3) == $@mapeventtarot$[0] ){
		for (.@i = 0; .@i < getarraysize($@tarot_participantes$); .@i++) {
		
			if ( $@tarot_participantes$[.@i] == strcharinfo(0) ){
				deletearray $@tarot_participantes$[.@i], 1;
				mapannounce $@mapeventtarot$[0], "[Evento Tar�]: ["+strcharinfo(0)+"] est� fora do evento!",bc_map;
				warp getsavepoint(0), getsavepoint(1), getsavepoint(2);
				sleep2 1500;
				atcommand "@alive";
				break;
			}
			
		}
	}
	end;
}

poring_w01,102,73,1	script	[GM] Cartomante	4_F_OPERATION,{
	end;
	
	OnSpeak:
	setarray .msgs$,
		"Usarei a habilidade 'Destino nas Cartas' em cada um!",
		"O vencedor ser� o �ltimo a sobreviver!",
		"Boa sorte a todos os participantes!",
		"TAROOOOOOW!";

	for (.@i = 0; .@i < getarraysize(.msgs$); .@i++ ){
		npctalk .msgs$[.@i];
		sleep 3000;
	}
	donpcevent "tarot_controller::OnReturn";
	end;
	
OnInit:
	setarray .x[0],94,105,99,110;
	setarray .y[0],68;
	npcspeed 195;
	initnpctimer;
	end;
	
	OnTimer1000:
	emotion rand(34,45);
	for(.c = 0; .c < getarraysize(.x); .c++){
		npcwalkto .x[.c],.y[0];
		sleep 3500;
	}
	initnpctimer;
	end;
}

function	script	resetEventoTarot	{

	// @param0 = array dos tarot_participantes
	// @param1 = map event
	deletearray getarg(0);
	$tarot_status = 0;
	mapwarp getarg(1), "prontera",156,180;
	return;
}

/* Mapflags */
poring_w01	mapflag	nowarp
poring_w01	mapflag	nowarpto
poring_w01	mapflag	noteleport
poring_w01	mapflag	monster_noteleport
poring_w01	mapflag	nosave
poring_w01	mapflag	nomemo
poring_w01	mapflag	nobranch
poring_w01	mapflag	nopenalty
poring_w01	mapflag	nomobloot
poring_w01	mapflag	nomvploot
poring_w01	mapflag	noicewall
/* MapFlags */