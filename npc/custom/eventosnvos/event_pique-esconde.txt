-	script	pique_contr	-1,{

// Inicio das Configura��es:
OnInit:
	.tempoEvento = 2;					// Tempo de dura��o do evento (Padr�o = [ 6 Minutos ] ).
	$@PiqueEsconde = 0;					// Vari�vel do evento (N�o mexer).
	$@PiqueEscEnter = 0;				// Vari�vel de entrada do evento (N�o mexer).
	.monster = 1002;					// ID do Monstro que ser� invocado.
	.anuncio = 3;						// Tempo de espera para iniciar evento (Padr�o = [ 3 Minutos ] ).
	.mapa$ = "brasilis";				// Mapa do evento.
	.premio = 7539;						// Premio.
	.quant = 2;							// Quantidade
	end;

OnStartEvent:
	$@PiqueEscEnter = 1;
	$@PiqueEsconde = 1;
	.time = .anuncio;
	for(.@i = 0; .@i < .time; .@i++){
		if($@PiqueEscEnter != 1) end;
		announce "[Pique Esconde]: O evento ir� come�ar em "+.anuncio+" minuto"+((.anuncio == 1)?"":"s")+". Use @evento para participar!",8;
		.anuncio--;
		sleep (60 * 1000);
	}
	announce "[Pique Esconde]: O evento come�ou!",8;
	.anuncio = .time;
	$@PiqueEscEnter = 0;
	donpcevent strnpcinfo(0)+"::OnBeginEvent";
	end;

OnBeginEvent:
	sleep 500;
	if(getmapusers(.mapa$) < 1){
		announce "[Pique Esconde]: Nenhum jogador compareceu ao evento, o evento foi finalizado!",0;
		$@PiqueEsconde = 0;
		end;
	}
	.timer = gettimetick(2) + (60 * .tempoEvento);
	mapannounce .mapa$,"[Pique Esconde]: Preparem-se voc�s tem "+.tempoEvento+" minutos para achar o mob escondido.",bc_map,0;
	donpcevent strnpcinfo(0)+"::OnMob";
		while($@PiqueEsconde){
			if(.timer < gettimetick(2) || getmapusers(.mapa$) == 0){
				announce "[Pique Esconde]: O evento chegou ao fim. Obrigado a todos que participaram!",8;
				sleep 500;
				//mapwarp .mapa$,"prontera",156,180;
				killmonsterall .mapa$;
				$@PiqueEsconde = 0;
			}
		sleep 10000;
		}
	end;

OnMob:
	monster .mapa$,0,0,"Escondido",.monster,1,strnpcinfo(0)+"::OnMyMobDead";
	end;
OnMyMobDead:
	announce "[Pique Esconde]: "+strcharinfo(0)+" foi o vencedor do evento. Obrigado a todos que participaram!",8;
	getitem .premio,.quant;
	//mapwarp .mapa$,"prontera",156,180;
	$@PiqueEsconde = 0;
	end;
}