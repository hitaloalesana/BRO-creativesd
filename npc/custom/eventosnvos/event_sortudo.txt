-	script	sort_controler	-1,{
// Inicio das Configura��es:
OnInit:
	.tempoEvento = 6;					// Tempo de dura��o do evento (Padr�o = [ 6 Minutos ] ).
	$@sortudo = 0;						// V�ri�vel do evento (N�o mexer).
	$@sortudoEnter = 0;					// V�ri�vel de entrada do evento (N�o mexer).
	.anuncio = 3;						// Tempo de espera para iniciar evento (Padr�o = [ 3 Minutos ] ).
	.mapa$ = "quiz_02";					// Mapa do evento.
	.tempoPvP = 3;						// Tempo de dura��o do PvP (Padr�o = [ 3 Minutos ] ).
	.tempoBau = 3;						// Tempo de dura��o do Ba� (Padr�o = [ 3 Minutos ] ).
	setarray .ST_Item,7539;					// Pr�mio ao quebrar Ba�.
	end;
	
OnStartEvent:
	$@sortudo = 1;
	$@sortudoEnter = 1;
	.time = .anuncio;
	for(.@i = 0; .@i < .time; .@i++) {
		if ($@sortudoEnter != 1) end;
		announce "[Sortudo]: O evento ir� come�ar em "+.anuncio+" minuto"+((.anuncio == 1)?"":"s")+". Use @evento para participar!",8;
		.anuncio--;
		sleep (60 * 1000);
	}
	announce "[Sortudo]: O evento come�ou!",8;
	$@sortudoEnter = 0;
	.anuncio = .time;
	donpcevent strnpcinfo(0)+"::OnBeginEvent";
	end;	

OnBeginEvent:
	sleep 5000;
	if (getmapusers(.mapa$) < 1) {
		announce "[Sortudo]: Nenhum jogador compareceu ao evento, o evento foi finalizado.",0;
		$@sortudo = 0;
		end;
	}	
	.timer = gettimetick(2) + (60 * .tempoEvento);
	mapannounce .mapa$,"[Sortudo]: Preparem-se! Voc�s tem "+.tempoEvento+" minutos para lutarem.",bc_map,0;
	donpcevent strnpcinfo(0)+"::OnPVP";
		while($@sortudo){
			if(.timer < gettimetick(2) || getmapusers(.mapa$) == 0){
				announce "[Sortudo]: O Evento chegou ao fim. Obrigado a todos que participaram!",8;
				sleep 5000;
				mapwarp .mapa$, "prontera", 156, 180;
				killmonsterall .mapa$;
				$@sortudo = 0;
				end;
			}
		sleep 10000;
		}	
	end;
OnPVP:
	pvpon .mapa$;
	sleep (1000 * 60 * .tempoPvP);
	pvpoff .mapa$;
	.@players = getareausers(.mapa$,314,57,359,34);
	areamonster .mapa$,314,57,359,34,"Tesouro Sortudo",1327,.@players,strnpcinfo(0)+"::OnTreasureDie";
	areamonster .mapa$,314,57,359,34,"Tesouro Sortudo",1327,.@players,strnpcinfo(0)+"::OnTreasureDie";
	mapannounce .mapa$,"[Sortudo]: Aten��o player vencedor(a)! Voc� tem pouco tempo para abrir o tesouro!!",bc_map,"0x00BFFF";
	sleep (1000 * 60 * .tempoBau);
	mapwarp .mapa$, "prontera", 156, 180;
	announce "[Sortudo]: O evento Terminou!! N�o pode participar? Tente na pr�xima!!",0,0x00BFFF;
	end;

OnTreasureDie:
	@id = rand(getarraysize(.ST_Item));
	getitem .ST_Item[@id],1;
	warp "prontera",156,191;
	end;	
}

/* Mapflags */
quiz_02	mapflag	nowarp
quiz_02	mapflag	nowarpto
quiz_02	mapflag	noteleport
quiz_02	mapflag	monster_noteleport
quiz_02	mapflag	nosave
quiz_02	mapflag	nomemo
quiz_02	mapflag	nobranch
quiz_02	mapflag	nopenalty
quiz_02	mapflag	nomobloot
quiz_02	mapflag	nomvploot
quiz_02	mapflag	noicewall
/* MapFlags */